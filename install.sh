#!/usr/bin/env bash

set -uo pipefail
trap 's=$?; echo "$0: Error on line "$LINENO": $BASH_COMMAND"; exit $s' ERR

exec 2> >(tee "stderr.log")

error() { clear; printf "%s\n" "$1" >&2; exit 1; }

installpkg() {
    pacman --noconfirm --needed -S "$1" >/dev/null 2>&1
}

message() {
    whiptail --clear --title "$1" --cancel-button "$2" --msgbox "$3" 10 60 3>&1 1>&2 2>&3
}

prompt_yes_no() {
    whiptail --clear  --title "$1" --ok-button "$2" --cancel-button "$3" --yesno "$4" 8 70 3>&1 1>&2 2>&3
}

menu() {
    local title="$1"; shift
    whiptail --clear --menu "$title" 0 0 0 "${options[@]}" 3>&1 1>&2 2>&3
    unset options
}

input() {
    local msg="$1"
    whiptail --clear --inputbox "$msg" 10 60 3>&1 1>&2 2>&3
}

prompt_input() {
    local input=$(input "Enter $1 $2")
    
    while [[ -z input ]]; do
	prompt_yes_no "Bad Input" "Try Again" "Quit" "$1 $2 cannot be empty." || error "User exited."
	unset input
	local input=$(input "Enter $1 $2")
    done
    
    echo "$input"
}

password() {
    local msg="$1"
    whiptail --clear --passwordbox "$msg" 10 60 3>&1 1>&2 2>&3
}

prompt_password() {
    local password=$(password "Enter $1 password")
    local confirmation=$(password "Enter $1 password confirmation")

    [[ -z $password ]] || [[ -z $confirmation ]]
    local empty=$?
    
    while [[ $empty -eq 0 ]] || [[ ! $password == $confirmation ]]; do
	if [[ $empty -eq 0 ]]; then
	    prompt_yes_no "Bad Password" "Try Again" "Quit" "Password cannot be empty." || error "User exited."
	else
	    prompt_yes_no "Bad Password" "Try Again" "Quit" "Passwords must match." || error "User exited."
	fi
	
	unset password
	unset confirmation
	unset empty
	
	local password=$(password "Enter $1 password")
	local confirmation=$(password "Enter $1 password confirmation")

	[[ -z $password ]] || [[ -z $confirmation ]]
	local empty=$?
    done
    
    echo "$password"
}

welcome() {
    cat <<EOF > /tmp/welcome.txt
                  /^----^
                  | 0  0 |
    Hoot!!        |  \/  |
                  /      |
      Hoot!!     |     |;;;|
                 |     |;;;|
                 |      \;;|
                  \       \|
-------------------(((--(((--------------------
|           Welcome To Gluax's Arch           |
|                  Installer                  |
---------------------|   |---------------------
                     |   |
                     |   |
                     |   |
EOF

    whiptail --clear --title "Welcome!" --ok-button "Start" --textbox "/tmp/welcome.txt"  0 0
    prompt_yes_no "Important Note!"  "All ready!" "Return..." "This installation will wipe your drive and all files. Are your sure you want to continue?." || error "User exited."
}

select_installation_drive() {
    local devices=$(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
    options=()
    while read -r name size; do
	    options=("${options[@]}" "$name" "$size")
    done <<< "$devices"

    local device=$(menu "Select Installtion Disk:")
    echo "$device"
}

partition_drive() {
    local drive="$1"
    message "Partioning Drive" "Ok" "Paritioning $drive..."
    
    # First set drive format as GPT.
    # Then make the EFI boot partition (1-513MiB).
    # Next the rest of the partition partition (513MiB-100%)
    # is resereved for LVM.
    # Following that we have to set the boot flag the first partition.
    # Finally we set LVM flag on the second partition.
    parted -s "$drive" \
	   mklabel gpt \
	   mkpart ESP fat32 1MiB 513MiB \
	   set 1 boot on \
	   mkpart primary ext2 513MiB 100% \
	   set 2 LVM on

    local p1="$(ls ${drive}* | grep -E "^${1}p?1$")"
    local p2="$(ls ${drive}* | grep -E "^${1}p?2$")"

    if [[ -z $p1 ]]; then
	message "Error" "Exit" "Failed to partion first part of drive. Exiting..."
	exit 1
    elif [[ -z $p2 ]]; then
	message "Error" "Exit" "Failed to partion first part of drive. Exiting..."
	exit 1
    fi
}

encrypt_drive() {
    local drive="$1"
    local part_lvm="$(ls ${drive}* | grep -E "^${1}p?2$")"
    
    if prompt_yes_no "Encrypt drive?" "Encrypt" "Security is lame" "Do you wish to encrypt your drive?"; then
	local password=$(prompt_password "drive encryption")
	
	# encrypt drive
	cryptsetup -c aes-xts-plain -y -s 512 luksFormat "$part_lvm" <(echo -en "$password") 3>&1 1>&2 2>&3
	# decrypt drive so we can continue operations
	echo -en "$password" | cryptsetup luksOpen "$part_lvm" luks <(echo -en "$password") 3>&1 1>&2 2>&3

	echo "/dev/mapper/luks:1"
    else
	echo "$part_lvm:0"
    fi
}

setup_lvm() {
    local part_lvm="$1"
    message "LVM Setup" "Ok" "Setting up lvm on $lvm..."
    
    pvcreate "$part_lvm"
    vgcreate vg0 "$part_lvm"

    # swap
    lvcreate -C y -L 16G vg0 -n swap
    # home
    lvcreate -C y -L 128G vg0 -n home
    #root
    lvcreate -l 100%free vg0 -n root

    # paranoid enable new volume
    vgchange -ay
}

mount_filesystems() {
    message "Mounting" "Ok" "Mounting FS..."
    local root="$1"
    local home="$2"
    local boot="$3"
    local swap="$4"
    
    mount "$root" /mnt
    mkdir -p /mnt/{boot,home}
    mount "$home" /mnt/home
    mount "$boot" /mnt/boot
    swapon "$swap"
}

format_filesystems() {
    local drive="$1"
    message "Formatting" "Ok" "Formatting filesystems..."
    
    local part_boot="$(ls ${drive}* | grep -E "^${1}p?1$")"
    local part_lvm="$(ls ${drive}* | grep -E "^${1}p?2$")"
    local part_swap="/dev/mapper/vg0-swap"
    local part_home="/dev/mapper/vg0-home"
    local part_root="/dev/mapper/vg0-root"
    
    mkfs.vfat -F32 "$part_boot"
    mkswap "$part_swap"
    mkfs.ext4 -L "Arch Home" "$part_home"
    mkfs.ext4 -L "Arch Root" "$part_root"

    mount_filesystems "$part_root" "$part_home" "$part_boot" "$part_swap"
    echo "$part_swap"
}

install_base() {
    message "Mirrorlist" "Ok" "Setting Mirrorlist Before Pacstrap..."
    echo 'Server = http://mirrors.kernel.org/archlinux/$repo/os/$arch' >> /etc/pacman.d/mirrorlist || error "Failed to"

    message "Pacstrapping" "Ok" "Pacstrapping base packages..."
    # base packages
    # libnewt is what will give the mounted portion access to whiptail(1)
    pacstrap /mnt base base-devel emacs fakeroot fish git libnewt lvm2 linux-lts linux-firmware man-db man-pages reflector sudo || error "Failed to pacstrap base packages."
}

unmount_filesystems() {
    message "Unmounting" "Ok" "Unmounting FS..."
    local encrypt=$1
    local swap="/dev/mapper/vg0-swap"
    
    umount -R /mnt
    swapoff "$swap"
    vgchange -an
    if [[ $encrypt -eq 1 ]]
    then
        cryptsetup luksClose luks
    fi
}

run_self_chroot() {
    local encrypt="$1"
    local drive="$2"
    message "Self-Chroot" "Ok" "Running self-chroot..."

    cp $0 /mnt/setup.sh
    arch-chroot /mnt ./setup.sh chroot $encrypt "$drive"

    if [[ ! -f /mnt/setup.sh ]]; then	
	message "Failure" "Ok" "Something failed inside the chroot, not unmounting filesystems so you can investigate."
	message "Notice" "Ok" "Make sure you unmount filesystems before tyring to run again."
    else
	message "Finishing up" "Ok" "Check error logs for errors..."
	rm /mnt/setup.sh
	mv /mnt/stderr.log ./setuperr.log
	unmount_filesystems $encrypt
    fi
}

setup_system () {
    # sync system clock
    timedatectl set-ntp true || error "Failed to sync system clock. Are you sure you're connected to the internet?"
    welcome
    pacman -Sy --noconfirm || error "Failed to update. Are you sure you're connected to the internet?"
    local drive=$(select_installation_drive)
    partition_drive "$drive"
    local lvm encrypt
    IFS=":"
    read lvm encrypt <<<"$(encrypt_drive "$drive")"
    setup_lvm "$lvm"
    format_filesystems "$drive" 
    install_base
    genfstab -U /mnt >> /mnt/etc/fstab || error "Failed to generate fstab."
    run_self_chroot "$encrypt" "$drive"
}

better_pacman() {
    message "Pretty Pacman" "Ok" "Making Pacman pretty..."
    # Make pacman colorful, concurrent downloads and Pacman eye-candy.
    grep -q "ILoveCandy" /etc/pacman.conf || sed -i "/#VerbosePkgLists/a ILoveCandy" /etc/pacman.conf
    sed -i "s/^#ParallelDownloads = 8$/ParallelDownloads = 5/;s/^#Color$/Color/" /etc/pacman.conf
}

set_timezone() {
    local timezone_names=$(timedatectl list-timezones)
    options=()
    while read tz; do
	options+=($tz "")
    done <<< "$timezone_names"
    local timezone=$(menu "Select a Timezone:")
    
    ln -sf "/usr/share/zoneinfo/$timezone" /etc/localtime || error "Failed to set timezone."
}

set_hosts_file() {
    local hostname="$1"
    message "Hosts File" "Ok" "Setting Hosts File..."

    cat <<EOF > /etc/hosts 
127.0.0.1 localhost.localdomain localhost $hostname
::1       localhost.localdomain localhost $hostname
EOF
}

set_hostname() {
    local hostname=$(prompt_input "hosts" "hostname")

    echo "$hostname" > /etc/hostname || error "Failed to set hostname."
    set_hosts_file "$hostname" || error "Failed to set hosts file."
}

set_locale() {
    message "Locale" "Ok" "Setting Locale as en_US.UTF-8"
    echo "LANG=en_US.UTF-8" >> /etc/locale.conf || error "Failed to set locale.conf."
    echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen || error "Failed to set locale.gen."
    locale-gen || error "Failed to locale-gen."
}

initramfs() {
    local encrypt=$1
    local drive="$2"
    local boot="$(ls ${drive}* | grep -E "^${1}p?1$")"
    message "INITRAMFS" "Ok" "Setting Mkinitcpio configurations..."
    
    if [[ $encrypt -eq 1 ]]; then
	sed -i 's/keyboard/keyboard encrypt lvm2/g' /etc/mkinitcpio.conf
    else
	sed -i 's/keyboard/keyboard lvm2/g' /etc/mkinitcpio.conf
    fi

    if [[ "$boot" == *nvme* ]]; then
	sed -i 's/MODULES=()/MODULES=(vmd)/g' /etc/mkinitcpio.conf
    fi

    mkinitcpio -P
}

set_root_password() {
    local root_password=$(prompt_password "root")
    echo -en "$root_password\n$root_password" | passwd
}

create_user () {
    local user=$(prompt_input "user" "username")
    local user_password=$(prompt_password "user")

    useradd -m -s "$(which fish)" -G adm,systemd-journal,wheel,rfkill,games,network,video,audio,optical,floppy,storage,scanner,power "$user"
    
    echo -en "$user:$user_password" | chpasswd
    echo "$user"
    # su -u "$user" -c "gpg --full-generate-key"
}

set_perms() {
    message "Userperms" "Ok" "Setting no password sudo commands"
    echo  "%wheel ALL=(ALL) ALL
%wheel ALL=(ALL) NOPASSWD: /usr/bin/shutdown,/user/bin/poweroff,/usr/bin/reboot,/usr/bin/systemctl suspend,/usr/bin/nmtui,/usr/bin/mount,/usr/bin/umount,/usr/bin/pacman -Syu,/usr/bin/pacman -Syyu,/usr/bin/systemctl restart NetworkManager,/usr/bin/rc-service NetworkManager restart,/usr/bin/pacman -Syyu --noconfirm,/usr/bin/loadkeys,/usr/bin/paru,/usr/bin/pacman -Syyuw --noconfirm" >> /etc/sudoers
}

set_make_cores() {
    message "Make Cores" "Ok" "Setting Make Cores for faster compilations..."
    # Use all cores for compilation.
    sed -i "s/-j2/-j$(nproc)/;s/^#MAKEFLAGS/MAKEFLAGS/" /etc/makepkg.conf
}

install_aur_helper() {
    local user="$1"
    options=("aura-bin" "" "pacaur" "" "paru" "" "yay-bin" "")
    local aur_helper=$(menu "Select an aur helper")
    local repodir="/home/$user/$aur_helper"

    if [[ "$aur_helper" == "pacaur" ]]; then
	pacman -Sy --noconfirm binutils expac fakeroot gcc git jq yajl make
	local ag="/home/$user/ag"
	sudo -u "$user" mkdir -p "$ag"
	sudo -u "$user" git clone --depth 1 "https://aur.archlinux.org/$auracle_git.git" "$ag" >/dev/null 2>&1 || { cd "$repodir" || return 1 ; sudo -u "$user" git pull --force origin master;}
	cd "$ag" > /dev/null 2>&1
	sudo -u "$user" makepkg --noconfirm -si > /dev/null 2>&1
	cd - > /dev/null 2>&1

	rm -rf "$ag"
    fi
    
    message "Aur Helper Installation" "Ok" "Installing \"$aur_helper\", an AUR helper. This may take a while..."
    sudo -u "$user" mkdir -p "$repodir"
    sudo -u "$user" git clone --depth 1 "https://aur.archlinux.org/$aur_helper.git" "$repodir" >/dev/null 2>&1 || { cd "$repodir" || return 1 ; sudo -u "$user" git pull --force origin master;}
    cd "$repodir" > /dev/null 2>&1
    sudo -u "$user" makepkg --noconfirm -si > /dev/null 2>&1
    cd - > /dev/null 2>&1
    
    local aur_helper=$(echo "$aur_helper" | sed 's/-bin//g')
    local path=$(which $aur_helper)
    echo "%wheel ALL=(ALL) NOPASSWD:$path" >> /etc/sudoers

    rm -rf "$repodir"
    echo "$aur_helper"
}

install() {
    local user="$1"
    local helper="$2"
    local aur="$3"
    local package="$4"

    case "$helper" in
	aura) if [[ -z "$aur" ]]; then
		  sudo -u "$user" sudo aura -S --noconfirm "$package"
	      else
		  sudo -u "$user" sudo aura -Axa --noconfirm "$package"
	      fi ;;
	pacaur) sudo -u "$user" sudo pacman -S --noedit --noconfirm "$package";;
	paru) sudo -u "$user" sudo paru -S --skipreview --noconfirm "$package";;
	yay) sudo -u sudo yay -S --nodiffmenu --noeditmenu --noconfirm "$package";;
    esac
}

install_custom() {
    local user="$1"
    local repodir="/home/$user/custom"
    local helper="$2"
    local programs="programs.csv"
    
    sudo -u "$user" mkdir -p "$repodir"
    cd "$repodir"
    sudo -u "$user" curl --proto '=https' --tlsv1.3 --remote-name -sSf https://gitlab.com/gluax/arch-configs/-/raw/master/programs.csv

    prompt_yes_no "Describe Packages?" "Yes" "No" "Do you want a description of the packages?"
    local describe=$?
    
    local aur program description
    while IFS=, read -r aur program description; do
	if [[ $describe -eq 0 ]]; then
	    message "Installing Package" "Ok" "$program: $description"
	fi
	install "$user" "$helper" "$aur" "$program"
    done <$programs
    cd -

    rm -rf "$repodir"
    # End of custom installations #
    ###########################################
    # Post installtion configurations #
    local home="/home/$user"

    message "Desktop Env" "Ok" "Setting i3 on start..."
    # Swaps caps lock and control.
    sudo -u "$user" cat <<EOF > "$home/.Xmodmap"
remove Lock = Caps_Lock
remove Control = Control_R
keysym Control_R = Caps_Lock
keysym Caps_Lock = Control_R
add Lock = Caps_Lock
add Control = Control_R
EOF
    sudo -u "$user" echo "/usr/bin/xmodmap $home/.Xmodmap" >> "$home/.xinitrc"
    sudo -u "$user" echo "exec i3" >> "$home/.xinitrc"
    sudo -u "$user" echo "xrandr -s 1920x1080" >> "$home/.xprofile"
    chown "$user" .xinitrc .Xdefaults .Xmodmap

    cat <<EOF > "/etc/lightdm/lightdm-gtk-greeter.conf"
[greeter]
theme-name = Arc-Dark
icon-theme-name = Papirus-Dark
background = #2f343f
EOF
    
    cd "$home"

    message "Re-Search" "Ok" "Building ctrl-r for Fish Shell..."
    git clone https://github.com/jbonjean/re-search
    cd re-search
    make
    mv re-search /usr/bin
    cd ..
    rm "$home/re-search"

    message "Emacs" "Ok" "Cloning Emacs configurations..."
    rm -rf "$home/.emacs.d"
    sudo -u "$user" git clone https://gitlab.com/gluaxspeed/emacs-conf.git .emacs.d

    message "Configs" "Ok" "Cloning configs..."
    rm -rf "$home/.config"
    sudo -u "$user" git clone https://gitlab.com/gluaxspeed/arch-configs.git .config
    # Todo figure out how to get lightdm resolution working
    # cp "$home/.config/add-and-set-resolution.sh /etc/lightdm/add-and-set-resolution.sh"

    # TODO maybe try to use systemd-nspawn since
    # services don't work in chroot
    # sudo -u "$user" systemctl enable --user emacs
    # sudo -u "$user"  systemctl start --user emacs

    # systemctl enable NetworkManager.service
    # systemctl start NetworkManager.service
}

setup_systemd_bootloader() {
    local drive="$1"
    local encrypt="$2"
    local luks="$(ls ${drive}* | grep -E "^${1}p?2$")"
    local uuid=$(blkid --match-tag UUID -o value "$luks")
    message "Bootloader" "Ok" "Setting Systemd bootloader..."
    
    bootctl --path=/boot install

    cat <<EOF >> /boot/loader/loader.conf
default arch-lts
timeout 3
editor 0
EOF

    # TODO figure out what to do here if it's not encrypted
    cat <<EOF > /boot/loader/entries/arch-lts.conf
title ArchLinux(LTS)
linux /vmlinuz-linux-lts
initrd /initramfs-linux-lts.img
options cryptdevice=UUID=${uuid}:luks root=/dev/mapper/vg0-root rw
EOF
}

configure_system() {
    local encrypt=$1
    local drive="$2"

    reflector --latest 20 --protocol https --sort rate --save /etc/pacman.d/mirrorlist || error "Failed to set best download mirrorlist. Are you sure you're connected to the internet?"
    better_pacman
    pacman -Syu --noconfirm || error "Failed to update. Are you sure you're connected to the internet?"
    set_timezone
    set_hostname
    set_locale
    initramfs $encrypt "$drive"
    set_root_password
    local user=$(create_user)
    set_perms
    set_make_cores
    local helper=$(install_aur_helper "$user")
    install_custom "$user" "$helper"
    setup_systemd_bootloader "$drive" "$encrypt"
}

if [[ "$#" -eq 3 ]] && [[ "$1" == "chroot" ]]; then
    configure_system $2 "$3"
else
    setup_system
fi

