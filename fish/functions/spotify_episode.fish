function spotify_episode
    notify-send -u critical --app-name=pass "Error: epi." --icon="$HOME/.config/notify-icons/error.png"
    set result (curl -s -X 'GET' https://api.spotify.com/v1/episodes/$TRACK_ID -H 'Accept: application/json' -H 'Content-Type: application/json' -H "Authorization:\"Bearer $argv[1]\"")
    set stat $status

    notify-send -u critical --app-name=pass "Error: aes  $TRACK_ID $result." --icon="$HOME/.config/notify-icons/error.png"
    
    if [ $stat -eq 0 ]
	echo $result | jq '.name, .show.name' | xargs printf "\"Playing '%s' from (podcast: '%s'.)\"" | xargs notify-send --urgency=low --expire-time=7000 --icon=$argv[2] --app-name=spotifyd spotifyd
    else
	return $status
    end
    
end
