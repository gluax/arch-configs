function leo_test --description "For testing leo"
    argparse 'c/clear' 'h/help' 'f/filter=?' 'm/module=?' 's/stdout' -- $argv
    
    if set -q _flag_c
	set -x CLEAR_LEO_TEST_EXPECTATIONS "true"
	echo "$CLEAR_LEO_TEST_EXPECTATIONS"
    end

    if set -q _flag_f
	set -x TEST_FILTER $_flag_f
	echo "$TEST_FILTER"
    end

    if set -q _flag_h
	echo "todo"
	return
    end

    set -l cmd "cargo test"
    if set -q _flag_m
	set cmd $cmd "-p $_flag_m"
    else
	set cmd $cmd "--all"
    end

    if set -q _flag_s
	set cmd $cmd "-- --nocapture"
    end

    eval $cmd
    
    set -e CLEAR_LEO_TEST_EXPECTATIONS
    set -e TEST_FILTER
end
