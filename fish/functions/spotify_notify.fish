function spotify_notify
    if ! type -q jq
	notify-send -u critical --app-name=fish "Error:" "jq is not installed." --icon="$HOME/.config/notify-icons/error.png"
	return 3
    end

    if ! type -q curl
	notify-send -u critical --app-name=fish "Error:" "curl is not installed." --icon="$HOME/.config/notify-icons/error.png"
	return 3
    end

    set user_id (lpass show spotify_client_id -j | jq .[].note)
    set secret_id (lpass show spotify_secret_id -j | jq .[].note)
    
    set token (curl -s -X 'POST' -u $user_id:$secret_id -d grant_type=client_credentials https://accounts.spotify.com/api/token | jq '.access_token' | cut -d\" -f2)
    set stat $status

    if [ "$PLAYER_EVENT" = "start" ]
	spotify_info $stat $token "/usr/share/icons/Adwaita/32x32/actions/media-playback-start-symbolic.symbolic.png"
    else if [ "$PLAYER_EVENT" = "change" ]
	spotify_info $stat $token "/usr/share/icons/Adwaita/32x32/actions/media-skip-forward-symbolic.symbolic.png"
    else if [ "$PLAYER_EVENT" = "stop" ]
	spotify_info $stat $token "/usr/share/icons/Adwaita/32x32/actions/media-playback-stop-symbolic.symbolic.png"
    else
	printf "Error: Unknown spotify event (%s)." $PLAYER_EVENT | xargs notify-send -u critical --icon="$HOME/.config/notify-icons/error.png" --app-name=fish
	return 3
    end
    
end
