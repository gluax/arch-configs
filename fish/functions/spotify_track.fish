function spotify_track
    notify-send -u critical --app-name=pass "Error: track." --icon="$HOME/.config/notify-icons/error.png"
    set result (curl -f -s -X 'GET' https://api.spotify.com/v1/tracks/$TRACK_ID -H 'Accept: application/json' -H 'Content-Type: application/json' -H "Authorization:\"Bearer $argv[1]\"")
    set stat $status

    # notify-send -u critical --app-name=pass "Error: ats $result." --icon="$HOME/.config/notify-icons/error.png"

    if [ $stat -eq 0 ]
	# set album_cover (echo $result | jq '.images[2].url')
	echo $result | jq '.name, .artists[].name, .album.name, .album.release_date, .track_number, .album.total_tracks' | xargs printf "\"Playing '%s' from '%s' (album: '%s' in %s (%s/%s))\"" | xargs notify-send --urgency=low --expire-time=7000 --icon=$argv[2] --app-name=spotifyd spotifyd
    else
	return $status
    end
    
end
