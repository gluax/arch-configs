function leo_debug
    if contains $fish_user_paths "$HOME/work/leo/target/debug"
	set --erase --universal fish_user_paths[1]
    else
	set -u fish_user_paths /home/gluax/work/leo/target/debug $fish_user_paths
    end
end
