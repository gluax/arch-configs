function spotify_info
    if [ $argv[1] -eq 0 ]
	set song (spotify_track $argv[2] $argv[3])
	set song_status $status

	set podcast (spotify_episode $argv[2] $argv[3])
	set podcast_status $status
 
	if [ song_status -ne 0  -a podcast_status -ne 0]
	    notify-send -u critical --app-name=fish "Error:" "Unknown track id." --icon="$HOME/.config/notify-icons/error.png"
	    return $song_status
	end
	    
    else
	notify-send -u critical --app-name=pass "Error: Unknown pass identifier." --icon="$HOME/.config/notify-icons/error.png"
	return $argv[1]
    end
end
