function random_file
    if [ (count $argv) -eq 1 ]
	set -l FILES $argv[1]/*
	set -l n (math --scale=0 (random)'%'(count $FILES)'+1')
     	echo $FILES[$n]
    else
     	echo "Error: Usage random_file dir."
     	echo "Given a directory randomly select a file from it."
    end
end
