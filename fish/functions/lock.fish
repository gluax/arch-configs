function lock
    if ! type -q i3
	notify-send -u critical --app-name=fish "Error:" "i3 is not installed." --icon="$HOME/.config/notify-icons/error.png"
	return 3
    end
	
    if type -q i3lock
	i3lock -i (random_file $HOME/.config/lock-images)
	return 0
    end
    
    notify-send -u critical --app-name=fish "Error:" "i3lock is not installed." --icon="$HOME/.config/notify-icons/error.png"
    return 3
end
